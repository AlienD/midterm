DROP TABLE IF EXISTS person CASCADE;
CREATE TABLE person (
    id SERIAL PRIMARY KEY,
    firstname VARCHAR(255),
    lastname VARCHAR(255),
    city VARCHAR(255),
    phone VARCHAR(255),
    telegram VARCHAR(255)
);
