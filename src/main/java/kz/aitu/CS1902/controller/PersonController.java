package kz.aitu.CS1902.controller;

import kz.aitu.CS1902.model.Person;
import kz.aitu.CS1902.service.PersonService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/v2/users")
public class PersonController {
    private static int i = 1;
    private final PersonService personService;

    public PersonController(PersonService personService) {
        this.personService = personService;
    }

    @GetMapping
    public ResponseEntity<?> getList(){
        return personService.getAll();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void deleteByID(@PathVariable("id") int id){
        personService.deleteByID(id);
    }

    @PostMapping
    public ResponseEntity<?> create(@RequestBody Person person){
        return ResponseEntity.ok(personService.save(person));
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> update(@RequestBody Person person){
        return ResponseEntity.ok(personService.save(person));
    }
}
