package kz.aitu.CS1902.service;

import kz.aitu.CS1902.model.Person;
import kz.aitu.CS1902.repository.PersonRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PersonService {
    private final PersonRepository personRepository;

    public PersonService(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(personRepository.findAll());
    }

    public ResponseEntity<?> getByID(int id){
        return ResponseEntity.ok(personRepository.findById(id));
    }

    public void deleteByID(int id){
        personRepository.deleteById(id);
    }

    public Person save(Person person){
        return personRepository.save(person);
    }

    public ResponseEntity<?> create(Person person){
        return ResponseEntity.ok(personRepository.create(person.getFirstname(), person.getLastname(), person.getCity(), person.getPhone(), person.getTelegram()));
    }
}
