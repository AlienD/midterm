package kz.aitu.CS1902;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Cs1902Application {

	public static void main(String[] args) {
		SpringApplication.run(Cs1902Application.class, args);
	}

}
