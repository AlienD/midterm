package kz.aitu.CS1902.repository;

import kz.aitu.CS1902.model.Person;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonRepository extends CrudRepository<Person, Integer> {
    @Query(value = "INSERT INTO person (firstname, lastname, city, phone, telegram) VALUES  (:firstname, :lastname, :city, :phone, :telegram)", nativeQuery = true)
    public ResponseEntity<?> create(String firstname, String lastname, String city, String phone, String telegram);
}
